// Importaciones 

import java.util.Scanner;
import java.util.logging.Logger;
import java.util.logging.FileHandler;
import java.util.logging.SimpleFormatter;

// Definición de las funciones 

public class Operaciones {
    
    public int laSuma (int numero1, int numero2){
    
        /* Función laSuma:

          - Descripción: Función que se encarga de realizar la operación de suma entre 2 números ingresados
                          por el usuario
                          
          - Inputs: * Primer número de la operación (numero1)  -> tipo de intput entero (int)
                    * Segundo número de la operación (numero2) -> tipo de intput entero (int)
                     
          - Outputs: Retorna el reultado de la suma de los 2 números ingresados por el usuario -> tipo de output entero (int)
        */ 

        return numero1 + numero2; // Mas recomendable por velocidad
    }

    public Double laSumaReal (Double numero1, Double numero2){
    
        /* Función laSumaReal:

          - Descripción: Función que se encarga de realizar la operación de suma entre 2 números ingresados
                          por el usuario
                          
          - Inputs: * Primer número de la operación (numero1)  -> tipo de intput entero (Double)
                    * Segundo número de la operación (numero2) -> tipo de intput entero (Double)
                     
          - Outputs: Retorna el reultado de la suma de los 2 números ingresados por el usuario -> tipo de output Double (Double)
        */ 

        return numero1 + numero2; // Mas recomendable por velocidad
    }

    public int laResta (int numero1, int numero2){
    
        /* Función laResta:
        
           - Descripción: Función que se encarga de realizar la operación de resta entre 2 números ingresados
                          por el usuario
                        
           - Inputs: * Primer número de la operación (numero1)  -> tipo de intput entero (int)
                     * Segundo número de la operación (numero2) -> tipo de intput entero (int)
                    
           - Outputs: Retorna el resultado de la resta de los 2 números ingresados por el usuario -> tipo de intput entero (int)
        */ 

        return numero1 - numero2; // Mas recomendable por velocidad
    }

    public int laMultipli (int numero1, int numero2){
    
        /* Función laMultipli:
        
           - Descripción: Función que se encarga de realizar la operación de multiplicación entre 2 números ingresados
                          por el usuario
                        
           - Inputs: * Primer número de la operación (numero1)  -> tipo de intput entero (int)
                     * Segundo número de la operación (numero2) -> tipo de intput entero (int)
                    
           - Outputs: Retorna el resultado de la multiplicación de los 2 números ingresados por el usuario -> tipo de intput entero (int)
        */ 

        return numero1 * numero2; // Mas recomendable por velocidad
    }

    public Double laPotencia (int numero1, int numero2){
    
        /* Función laPotencia:
        
           - Descripción: Función que se encarga de realizar la operación de potenciación para un numero ingresa por el usuario,
                          donde toma la siguiente estructura numero1^numero2
                        
           - Inputs: * Primer número de la operación (numero1)  -> tipo de intput entero (int)
                     * Segundo número de la operación (numero2) -> tipo de intput entero (int)
                    
           - Outputs: Retorna el resultado de la potenciación de los 2 números ingresados por el usuario -> tipo de output Double (Double)
        */ 

        return Math.pow(numero1, numero2); // Mas recomendable por velocidad
    }

    public Double laRaiz (int numero1, Double numero2){
    
        /* Función laRaiz:
        
           - Descripción: Función que se encarga de realizar la operación de raiz para un numero ingresa por el usuario,
                          donde toma la siguiente estructura numero1^(1/numero2)
                        
           - Inputs: * Primer número de la operación (numero1)  -> tipo de intput entero (int)
                     * Segundo número de la operación (numero2) -> tipo de intput entero (int)
                    
           - Outputs: Retorna el resultado de la potenciación de los 2 números ingresados por el usuario -> tipo de output Double (Double)
        */ 

        return Math.pow(numero1, (1/numero2)); // Mas recomendable por velocidad
    }

    public int selectorOperacionInt (String mathSymbol, int numero1, int numero2){

        /* Función selectorOperacionInt:

           - Descripción: Función que se encarga de llamar a las funciones laSuma, laResta y laMultipli según lo solicite el usuario y
                          retorna el valor resultante de la operación

           - Inputs: * mathSymbol -> tipo de intput cadena (char[] -> String)
                     * Primer número de la operación (numero1)  -> tipo de intput entero (int)
                     * Segundo número de la operación (numero2) -> tipo de intput entero (int)

           - Outputs: Retorna el resultado de la operación deseada por el usuario de los 2 números ingresados por el usuario -> tipo de output entero (int)
        */

        int respuesta = 0;

        if (mathSymbol.equals("+")){

            respuesta = laSuma(numero1, numero2);

        } else if (mathSymbol.equals("-")) {

            respuesta = laResta(numero1, numero2);

        } else if (mathSymbol.equals("*")) {
            
            respuesta = laMultipli (numero1, numero2);

        } 

        return respuesta; 
    }

    public Double selectorOperacionDbl (String mathSymbol, int numero1, int numero2){

        /* Función selectorOperacionDbl:

        - Descripción: Función que se encarga de llamar a las funciones laSuma, laResta y laMultipli según lo solicite el usuario y
                          retorna el valor resultante de la operación
                          
           - Inputs: * mathSymbol -> tipo de intput cadena (char[] -> String)
                     * Primer número de la operación (numero1)  -> tipo de intput entero (int)
                     * Segundo número de la operación (numero2) -> tipo de intput entero (int)

           - Outputs: Retorna el resultado de la operación deseada por el usuario de los 2 números ingresados por el usuario -> tipo de output Double (Double)
        */

        Double respuesta = 0.0;

        if (mathSymbol.equals("^")){

            respuesta = laPotencia(numero1, numero2);

        } else if (mathSymbol.equals("^/")) {

            respuesta = laRaiz(numero1, Double.valueOf(numero2));

        }

        return respuesta; 
    }

    public void calculadora(Operaciones misOperaciones){

        /* Función calculadora:

           - Descripción: Función que se encarga de integrar todas las funciones relacionadas a la suma o resta de los 2 número dados por
                          el usuario y es la enargada de generar el loop para realizar multiples iteraciones
                        
           - Inputs: misOperaciones -> Clase Operaciones
        */

        // Inicialización de variables

        String  accion      = "+";

        while (!accion.equals("")) {

            Scanner input = new Scanner(System.in);

            System.out.println("Ingrese el signo de la operación que desea realizar, las opciones son:");
            System.out.println("o) +       -> Para la suma");
            System.out.println("o) -       -> Para la resta");
            System.out.println("o) *       -> Para la multiplicación");
            System.out.println("o) ^       -> Para la potenciación");
            System.out.println("o) +r      -> Para la suma de reales");
            System.out.println("o) ^/      -> Para la raiz");
            System.out.println("o) Oprima la tecla enter para terminar los procesos");

            accion = input.nextLine().trim().toUpperCase();

            if (accion.equals("")) {
                
                System.out.println("Hasta luego\n");
                
            } else if (accion.equals("+") || accion.equals("-") || accion.equals("*")) {

                int numero1;
                int numero2;
                int respuesta;

                numero1 = inputUserNumInt(1);
                numero2 = inputUserNumInt(2);
                
                // Se llama la función

                respuesta   = misOperaciones.selectorOperacionInt(accion, numero1, numero2);

                System.out.println("\n" + Integer.toString(numero1) + " " + accion + " " + Integer.toString(numero2) + " = " + respuesta + "\n");

            } else if (accion.equals("^") || accion.equals("^/")) {

                int numero1;
                int numero2;
                Double respuesta;
                
                numero1 = inputUserNumInt(1);
                numero2 = inputUserNumInt(2);

                respuesta = misOperaciones.selectorOperacionDbl(accion, numero1, numero2);

                System.out.println("\n" + Integer.toString(numero1) + " " + accion + " " + Integer.toString(numero2) + " = " + respuesta + "\n");
            
            } else if (accion.equals("+R")) {

                Double numero1;
                Double numero2;
                Double respuesta;

                numero1 = inputUserNumDbl(1);
                numero2 = inputUserNumDbl(2);

                respuesta = laSumaReal (numero1, numero2);

                System.out.println("\n" + Double.toString(numero1) + " + " + Double.toString(numero2) + " = " + respuesta + "\n");

            } else {

                System.out.println("\nLa operación no se conoce, vuelva a intentarlo\n");
                
            }

        }

    }

    public int inputUserNumInt(int posicion) {

        /* Función inputUserNumInt:

           - Descripción: Función que se encarga de solicitar al usuario un input entero
                        
           - Inputs: posicion -> Tipo entero (int)

           - Output: returna el input del usuario -> Tipo entero
        */
        
        Scanner input = new Scanner(System.in);
        
        int numero;

        do {
            
            System.out.println("\nIngrese el número " + posicion + " de la operación");
            
            String auxInput = input.nextLine();

            try{

                numero = Integer.parseInt(auxInput);
                
            } catch (Exception e) {

                System.out.println("El input falló en el input del número, ya que no tiene el formato correcto, vuelva a intentarlo");
                erroLogFile(e.toString());
                
                numero = -1;
                
            }

        } while(numero < 0);

        return numero;
    }

    public Double inputUserNumDbl(int posicion) {

        /* Función inputUserNumDbl:

           - Descripción: Función que se encarga de solicitar al usuario un input double
                        
           - Inputs: posicion -> Tipo entero (int)

           - Output: returna el input del usuario -> Tipo entero 
        */
        
        Scanner input = new Scanner(System.in);
        
        Double numero = null;

        while(numero == null){
            
            System.out.println("\nIngrese el número " + posicion + " de la operación");
            
            String auxInput = input.nextLine();

            try{

                numero = Double.parseDouble(auxInput);
                
            } catch (Exception e) {

                System.out.println("El input falló en el input del número, ya que no tiene el formato correcto, vuelva a intentarlo");
                erroLogFile(e.toString());
                
            }

        }

        return numero;
    }

    public void erroLogFile(String err){

        /* Función erroLogFile:

           - Descripción: Función que se encarga de almacenar los log de los errores de ejecución que se tienen al momento
                          de ejecutar el programa
                        
           - Inputs: err (error) -> Tipo String
        */

        Logger logger = Logger.getLogger("MyLog");  
        FileHandler fh;  
        
        try {
            String ruta = System.getProperty("user.dir") + "\\errorLog.log";

            fh = new FileHandler(ruta, true);  
            logger.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();  
            fh.setFormatter(formatter);  
  
            logger.info(err);  

        } catch (Exception e) {

            System.out.println("Ocurrió un error");
        }

    }

}
